DROP DATABASE IF EXISTS exercise;
CREATE DATABASE exercise;
USE exercise;

/*------------------------------------------------------------------------------------------------
--                                  SANDWITCHES PROBLEM
------------------------------------------------------------------------------------------------*/

DROP TABLE IF EXISTS tastes;
CREATE TABLE tastes (
  name VARCHAR(20),
  filling VARCHAR(20)
);

DROP TABLE IF EXISTS locations;
CREATE TABLE locations (
  lname VARCHAR(20),
  phone VARCHAR(10),
  address VARCHAR(20)
);

DROP TABLE IF EXISTS sandwitches;
CREATE TABLE sandwitches (
  location VARCHAR(20),
  bread VARCHAR(20),
  filling VARCHAR(20),
  price FLOAT
);

ALTER TABLE tastes
ADD CONSTRAINT pk_NameFilling PRIMARY KEY (name, filling);

ALTER TABLE locations
ADD CONSTRAINT pk_Lname PRIMARY KEY (lname);

ALTER TABLE sandwitches
ADD CONSTRAINT pk_LocBreadFilling PRIMARY KEY (location, bread, filling),
ADD CONSTRAINT fk_SandwitchesLocations FOREIGN KEY (location) REFERENCES locations(lname);

-- Display schema and contraints
DESCRIBE tastes;
DESCRIBE locations;
DESCRIBE sandwitches;

-- Load sample data into tables.
INSERT INTO tastes VALUES ('Brown','Turkey');
INSERT INTO tastes VALUES ('Brown','Beef');
INSERT INTO tastes VALUES ('Brown','Ham');
INSERT INTO tastes VALUES ('Jones','Cheese');
INSERT INTO tastes VALUES ('Green','Beef');
INSERT INTO tastes VALUES ('Green','Turkey');
INSERT INTO tastes VALUES ('Green','Cheese');

INSERT INTO locations VALUES ('Lincoln','683 4523','Lincoln Place');
INSERT INTO locations VALUES ('O\'Neill\'s','674 2134','Pearse St');
INSERT INTO locations VALUES ('Old Nag','767 8132','Dame St');
INSERT INTO locations VALUES ('Buttery','702 3421','College St');

INSERT INTO sandwitches VALUES ('Lincoln','Rye','Ham',1.25);
INSERT INTO sandwitches VALUES ('O\'Neill\'s','White','Cheese',1.20);
INSERT INTO sandwitches VALUES ('O\'Neill\'s','Whole','Ham',1.25);
INSERT INTO sandwitches VALUES ('Old Nag','Rye','Beef',1.35);
INSERT INTO sandwitches VALUES ('Buttery','White','Cheese',1.00);
INSERT INTO sandwitches VALUES ('O\'Neill\'s','White','Turkey',1.35);
INSERT INTO sandwitches VALUES ('Buttery', 'White', 'Ham', 1.10);
INSERT INTO sandwitches VALUES ('Lincoln', 'Rye', 'Beef', 1.35);
INSERT INTO sandwitches VALUES ('Lincoln', 'White', 'Ham', 1.30);
INSERT INTO sandwitches VALUES ('Old Nag', 'Rye', 'Ham', 1.40);

-- Display data in each table.
SELECT * FROM tastes;
SELECT * FROM locations;
SELECT * FROM sandwitches;

-- places where Jones can eat (using a nested subquery)
SELECT location AS 'Places where Jones can eat' FROM sandwitches WHERE filling IN (SELECT filling FROM tastes WHERE name = 'Jones');

-- places where Jones can eat (without using a nested subquery)
SELECT location AS 'Places where Jones can eat' FROM tastes INNER JOIN sandwitches
ON tastes.filling = sandwitches.filling
WHERE tastes.name = 'Jones';

-- for each location the number of people who can eat there.
SELECT location, COUNT(DISTINCT name) AS 'number of people who can eat here'
FROM tastes INNER JOIN sandwitches
ON tastes.filling = sandwitches.filling
GROUP BY location;

/*------------------------------------------------------------------------------------------------
--                                  MULTIBRANCH LIBRARY PROBLEM
------------------------------------------------------------------------------------------------*/

DROP TABLE IF EXISTS branches;
CREATE TABLE branches (
  bcode VARCHAR(5),
  librarian VARCHAR(20),
  address VARCHAR(20)
);

DROP TABLE IF EXISTS titles;
CREATE TABLE titles (
  title VARCHAR(20),
  author VARCHAR(20),
  publisher VARCHAR(20)
);

DROP TABLE IF EXISTS holdings;
CREATE TABLE holdings (
  branch VARCHAR(5),
  title VARCHAR(20),
  `#copies` INT
);

ALTER TABLE branches
ADD CONSTRAINT pk_Bcode PRIMARY KEY (bcode);

ALTER TABLE titles
ADD CONSTRAINT pk_Title PRIMARY KEY (title);

ALTER TABLE holdings
ADD CONSTRAINT pk_BranchTitle PRIMARY KEY (branch, title),
ADD CONSTRAINT fk_HoldingsBranches FOREIGN KEY (branch) REFERENCES branches(bcode),
ADD CONSTRAINT fk_HoldingsTitles FOREIGN KEY (title) REFERENCES titles(title);

-- Display schema and contraints
DESCRIBE branches;
DESCRIBE titles;
DESCRIBE holdings;

-- Load sample data into tables.
INSERT INTO branches VALUES ('B1', 'John Smith', '2 Anglesea Rd');
INSERT INTO branches VALUES ('B2', 'Mary Jones', '34 Pearse St');
INSERT INTO branches VALUES ('B3', 'Francis Owens', 'Grange X');

INSERT INTO titles VALUES ('Susannah', 'Ann Brown', 'Macmillan');
INSERT INTO titles VALUES ('How to Fish', 'Amy Fly', 'Stop Press');
INSERT INTO titles VALUES ('A History of Dublin', 'David Little', 'Wiley');
INSERT INTO titles VALUES ('Computers', 'Blaise Pascal', 'Applewoods');
INSERT INTO titles VALUES ('The Wife', 'Ann Brown', 'Macmillan');

INSERT INTO holdings VALUES ('B1', 'Susannah', 3);
INSERT INTO holdings VALUES ('B1', 'How to Fish', 2);
INSERT INTO holdings VALUES ('B1', 'A History of Dublin', 1);
INSERT INTO holdings VALUES ('B2', 'How to Fish', 4);
INSERT INTO holdings VALUES ('B2', 'Computers', 2);
INSERT INTO holdings VALUES ('B2', 'The Wife', 3);
INSERT INTO holdings VALUES ('B3', 'A History of Dublin', 1);
INSERT INTO holdings VALUES ('B3', 'Computers', 4);
INSERT INTO holdings VALUES ('B3', 'Susannah', 3);
INSERT INTO holdings VALUES ('B3', 'The Wife', 1);

-- Display data in each table.
SELECT * FROM branches;
SELECT * FROM titles;
SELECT * FROM holdings;

-- the names of all library books published by Macmillan.
SELECT title AS 'Books published by Macmillan' FROM titles WHERE publisher = 'Macmillan';

-- branches that hold any books by Ann Brown (using a nested subquery).
SELECT DISTINCT branch AS 'Branches that hold any books by Ann Brown' FROM holdings WHERE title IN (SELECT title FROM titles WHERE author = 'Ann Brown');

-- branches that hold any books by Ann Brown (without using a nested subquery).
SELECT DISTINCT branch AS 'Branches that hold any books by Ann Brown'
FROM holdings INNER JOIN titles
ON holdings.title = titles.title;

-- the total number of books held at each branch.
SELECT branch, SUM(`#copies`) AS 'total number of books held at this branch' FROM holdings GROUP BY branch;